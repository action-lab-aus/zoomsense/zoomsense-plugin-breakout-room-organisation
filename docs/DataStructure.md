# Breakout Room Organisation Data Structure

When using this Plugin, define sections like so:

```yaml
schedule:
  script:
    - name: "Example Breakout Room Organisation"
      advance:
        type: manual
      plugins:
        - plugin: Task
          settings:
            breakout rooms:
              # Four breakout rooms with participants assigned randomly (but as evenly as possible).
              # Call these breakout rooms A, B, C and D, for the purposes of these comments, and the
              # users assigned to room A users A1, A2, A3 and so on.
              type: random
              room_count: 4
            messages:
              - name: Discussion
                content: "Please discuss this topic: ..."
                config:
                  from: manual
                  type: room
                  sensors: ZoomSensor_1
    - name: "Example Round Robin Rotation 1"
      advance:
        type: auto
      plugins:
        - plugin: Task
          settings:
            advance:
              # Start with the timer paused, but once it's started from the dashboard, the round will last 92 seconds.
              type: paused
              duration: 92 seconds
            breakout rooms:
              # Users will be paired, with each user assigned to odd-numbered breakout rooms (A and C) in
              # the original "random" assignment paired with a user from an even-numbered breakout room (B and D).
              # User A1 will be paired with user B1, A2 with B2, C1 with D1 and so on. 
              type: transition
              transition: "Pairs Round Robin"
            messages:
              - name: Instructions
                content: "In this breakout room, you should..."
                config:
                  from: end
                  time: 90 seconds
                  type: broadcast
    - name: "Example Round Robin Rotation 2"
      advance:
        type: auto
      plugins:
        - plugin: Task
          settings:
            advance:
              type: paused
              duration: 92 seconds
            breakout rooms:
              # Each additional "Pairs Round Robin" transition will rotate the pairs.  Since this is the second "Pairs
              # Round Robin" transition, user A1 will be paired with B2, user A2 with B3 etc.  The last user from room A
              # will be paired with B1.
              type: transition
              transition: "Pairs Round Robin"
            messages:
              - name: Instructions
                content: "In this breakout room, you should..."
                config:
                  from: end
                  time: 90 seconds
                  type: broadcast
```
