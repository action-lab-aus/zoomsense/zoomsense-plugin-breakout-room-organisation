export type MessageConfig = {
  config: {
    from: string;
    time?: string | undefined;
    type: string;
    sensors?: string | undefined;
  };
  content: string;
  name: string;
  sent: number | undefined | null;
  send?: true;
  sendJobId?: string;
};

const isMessageConfigConfig = (
  value: unknown
): value is MessageConfig["config"] => {
  if (typeof value !== "object" || !value) return false;
  const asRecord = value as Record<string, unknown>;
  return (
    typeof asRecord["from"] === "string" &&
    typeof asRecord["type"] === "string" &&
    (asRecord["time"] === undefined || typeof asRecord["time"] === "string") &&
    (asRecord["sensor"] === undefined ||
      typeof asRecord["sensor"] === "string") &&
    (asRecord["paused"] === undefined ||
      typeof asRecord["paused"] === "boolean")
  );
};

export const isMessageConfig = (value: unknown): value is MessageConfig => {
  if (typeof value !== "object" || !value) return false;
  const asRecord = value as Record<string, unknown>;
  return (
    typeof asRecord["name"] === "string" &&
    typeof asRecord["content"] === "string" &&
    (asRecord["sent"] === undefined ||
      asRecord["sent"] === null ||
      typeof asRecord["sent"] === "number") &&
    (asRecord["send"] === undefined ||
        asRecord["send"] === true) &&
    isMessageConfigConfig(asRecord["config"])
  );
};
