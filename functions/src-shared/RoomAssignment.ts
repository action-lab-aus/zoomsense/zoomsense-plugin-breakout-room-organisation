import { ZoomSenseCurrentUserInMeeting } from "@zoomsense/zoomsense-firebase";

export type RoomAssignment = Record<string, ZoomSenseCurrentUserInMeeting[]>;
