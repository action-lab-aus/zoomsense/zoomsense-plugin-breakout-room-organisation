import { RoomAssignment } from "./RoomAssignment";

export type Topology = {
  type: string;
  // eslint-disable-next-line camelcase
  room_count?: undefined | number;
  // eslint-disable-next-line camelcase
  participants_per_room?: undefined | number;
  // eslint-disable-next-line camelcase
  same_as?: string | undefined;
  assignment?: RoomAssignment | undefined;
};
