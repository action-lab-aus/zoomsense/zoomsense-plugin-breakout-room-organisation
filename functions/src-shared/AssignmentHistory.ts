import { RoomAssignment } from "./RoomAssignment";

export type AssignmentHistoryEntry = {
  assignment: RoomAssignment;
  section: number;
  timestamp: number;
};
