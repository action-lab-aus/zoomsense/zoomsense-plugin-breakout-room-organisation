import * as functions from "firebase-functions";

import { db } from "./db";
import { isMessageConfig } from "../src-shared";
import { pushNewMessageHistoryEvent } from "./history";

const HttpsError = functions.https.HttpsError;

// Create a new message in the current section
export const newMessage = functions.https.onCall(async (data, context) => {
  if (!context.auth?.token) {
    return new HttpsError("unauthenticated", "unauthenticated");
  }
  const { message, meetingId } = data;

  if (!isMessageConfig(message)) {
    return new HttpsError("invalid-argument", "Invalid/Malformed message");
  }

  const currentMessages = (
    await db.ref(`config/${meetingId}/current/currentState/plugins/task/messages`).get()
  ).val();

  const currentMessageCount = currentMessages ? currentMessages.length : 0;

  await db
    .ref(
      `config/${meetingId}/current/currentState/plugins/task/messages/${currentMessageCount}`
    )
    .set(message);

  await pushNewMessageHistoryEvent(meetingId, message);

  return "OK";
});
