import * as functions from "firebase-functions";
import { assignUsers } from "./topologies";
import { db } from "./db";
import { typedFirebaseFunctionRef } from "@zoomsense/zoomsense-firebase-server";

export const nextSection = typedFirebaseFunctionRef(functions.database.ref)(
  "/config/{meetingId}/current/currentState/sectionLoaded"
).onUpdate(async (_change, context) => {
  const meetingId = context.params.meetingId;

  const config = (
    await db.ref(`config/${meetingId}/current/currentState/plugins/task`).get()
  ).val();

  if (!config?.enabled) {
    return;
  }

  await assignUsers(meetingId, config["breakout rooms"]);
});
