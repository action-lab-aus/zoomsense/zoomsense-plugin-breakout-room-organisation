import { typedFirebaseFunctionRef } from "@zoomsense/zoomsense-firebase-server";
import * as functions from "firebase-functions";
import { db } from "./db";

// Watch the currentUsers datastructure for name changes, and update the current assignment
// if when somebody changes their name
export const handleRename = typedFirebaseFunctionRef(functions.database.ref)(
  "data/currentUsers/{meetingId}/{sensorId}/{userId}"
).onUpdate(async (change, context) => {
  const { meetingId, userId } = context.params;

  const enabled = (
    await db
      .ref(`config/${meetingId}/current/currentState/plugins/task/enabled`)
      .get()
  ).val();

  if (!enabled) {
    return;
  }

  // Only care about name changes
  const newName = change.after.val().username;
  if (change.before.val().username === newName) {
    return;
  }

  const breakoutRoomsRef = db.ref(
    `config/${meetingId}/current/currentState/plugins/task/breakout rooms`
  );

  const breakoutRooms = (await breakoutRoomsRef.get()).val();
  if (!breakoutRooms?.assignment) {
    return;
  }

  // Here we map over each user in each room, looking for the user who changed
  // their name. When we find them, we update the assignment to have the new name
  const assignmentWithNameUpdated = Object.fromEntries(
    Object.entries(breakoutRooms.assignment).map(([roomName, users]) => [
      roomName,
      users.map((user) =>
        user.userId === userId ? { ...user, username: newName } : user
      ),
    ])
  );

  await breakoutRoomsRef.update({
    assignment: assignmentWithNameUpdated,
  });
});
