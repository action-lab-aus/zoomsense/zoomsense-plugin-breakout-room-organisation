import { EntryExitEvent } from "@zoomsense/zoomsense-firebase";

import { db } from "./db";
import { assignUsers } from "./topologies";

// Given an entry exit log, if the entry exit log represents a user joining
// a room, and the user hasn't currently been assigned to a room, run the
// assign function again
export const assignParticipantIfLateJoiner = async (
  entryExit: EntryExitEvent,
  meetingId: string
): Promise<void> => {
  // We only care about users joining the room
  if (entryExit.status !== "enter") {
    return;
  }

  const breakoutRooms = (
    await db
      .ref(
        `config/${meetingId}/current/currentState/plugins/task/breakout rooms`
      )
      .get()
  ).val();

  if (!breakoutRooms?.assignment) {
    return;
  }

  const assignedUsers = Object.values(breakoutRooms.assignment).flat();

  if (!assignedUsers.length) {
    // If we don't have any assignments anyway, run the assigner from scratch.
    await assignUsers(meetingId, breakoutRooms);
    return;
  }
  // If the user is a host or has already been assigned - we're done here
  if (
    entryExit.userRole !== "Attendee" ||
    assignedUsers.find((user) => user.username === entryExit.username)
  ) {
    return;
  }
  // Otherwise the user is a late joiner, let's assign them to the room with the least
  // number of users
  const roomToAssignTo = Object.entries(breakoutRooms.assignment).sort(
    ([_, a], [__, b]) => a.length - b.length
  )[0]?.[0];

  const assignment = {
    ...breakoutRooms.assignment,
    [roomToAssignTo]: breakoutRooms.assignment[roomToAssignTo].concat([
      {
        username: entryExit.username,
        userRole: entryExit.userRole,
        userId: entryExit.uid,
      },
    ]),
  };

  await db
    .ref(
      `/config/${meetingId}/current/currentState/plugins/task/breakout rooms`
    )
    .update({
      assignment,
    });
};
