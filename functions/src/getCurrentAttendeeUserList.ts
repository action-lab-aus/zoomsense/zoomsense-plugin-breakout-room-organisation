import { ZoomSenseCurrentUserInMeeting } from "@zoomsense/zoomsense-firebase";

import { db } from "./db";

/**
 * Get the list of all Attendees currently in the meeting.
 *
 * @param {string} meetingId id of the meeting to get the list for
 */
export const getCurrentAttendeeUserList = async (
  meetingId: string
): Promise<ZoomSenseCurrentUserInMeeting[]> => {
  const usersForMeeting = (
    await db.ref(`data/currentUsers/${meetingId}`).get()
  ).val();
  return Object.values(usersForMeeting ?? {})
    .flatMap((users) => (Object.values(users)))
    .filter((user) => user.userRole === "Attendee");
};
