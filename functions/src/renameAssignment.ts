import * as functions from "firebase-functions";
import { roomWithNameExists, updateAssignmentName } from "./db";
const HttpsError = functions.https.HttpsError;

export const renameAssignment = functions.https.onCall(
  async (data, context) => {
    if (!context.auth?.token) {
      return new HttpsError("unauthenticated", "unauthenticated");
    }

    const { meetingId, oldName, newName } = data;

    // If the room we are renaming doesn't exist, return a argument error
    const oldRoomExists = await roomWithNameExists(meetingId, oldName);
    if (!oldRoomExists) {
      return new HttpsError(
        "invalid-argument",
        `Breakout Room with name "${oldName}" doesn't exist`
      );
    }

    // If a room with the new name already exists, return an argument error
    const newRoomExists = await roomWithNameExists(meetingId, newName);
    if (newRoomExists) {
      return new HttpsError(
        "invalid-argument",
        `Breakout Room with name "${newName}" already exists`
      );
    }

    const result = await updateAssignmentName(meetingId, oldName, newName);
    if (result.isErr) {
      console.error(result.err);
      return new HttpsError("internal", "internal");
    }

    return "OK";
  }
);
