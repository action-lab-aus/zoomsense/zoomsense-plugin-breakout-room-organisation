import * as admin from "firebase-admin";
import {
  cancelScheduledFirebaseSet,
  createScheduledFirebaseSet,
} from "@zoomsense/zoomsense-firebase";
import { ZoomSenseServerFirebaseDB } from "@zoomsense/zoomsense-firebase-server";

import { err, ErrorOr, MessageConfig, RoomAssignment, succ } from "../src-shared";

export const db = new ZoomSenseServerFirebaseDB(admin.database());

const getValFromDb = async <P extends string>(path: P) =>
  (await db.ref(path).get()).val();

export const getCurrentSectionEndTime = async (
  meetingId: string
): Promise<ErrorOr<number | null>> => {

  const endsAt = await getValFromDb(
    `/config/${meetingId}/current/currentState/endsAt`
  );

  if (endsAt !== null && typeof endsAt !== "number") {
    return err(
      `[${meetingId}]: Current section end time in Database is invalid/malformed: "${endsAt}". (Expected a number or null)`
    );
  }

  return succ(endsAt);
};

export const setSentFlagForMessage = async (
  meetingId: string,
  messageIndex: number
): Promise<void> => {
  await db
    .ref(
      `/config/${meetingId}/current/currentState/plugins/task/messages/${messageIndex}/sent`
    )
    .set(Date.now());
  await db
    .ref(
      `config/${meetingId}/current/currentState/plugins/task/messages/${messageIndex}/send`
    )
    .remove();
};

export const setMessage = async (
  meetingId: string,
  messageIndex: number,
  message: MessageConfig
): Promise<void> => {
  await db
    .ref(
      `/config/${meetingId}/current/currentState/plugins/task/messages/${messageIndex}`
    )
    .update(message);
};

// Get a list of the IDs of ZoomSensors that are currently in breakout rooms
export const getZoomSensorsInBreakoutRooms = async (
  meetingId: string
): Promise<string[]> => {
  const activeSpeakers = await getValFromDb(`data/chats/${meetingId}`);

  if (!activeSpeakers || typeof activeSpeakers !== "object") return [];

  return Object.entries(activeSpeakers)
    .filter(([, { isInBO }]) => isInBO)
    .map(([zoomSensor]) => zoomSensor);
};

export const setBreakoutRoomAssignment = async (
  meetingId: string,
  assignment?: RoomAssignment
): Promise<void> => {
  await db
    .ref(
      `/config/${meetingId}/current/currentState/plugins/task/breakout rooms`
    )
    .update({ assignment });
};

// Request that sensor with ID `sensorId` broadcasts the given message
// (i.e. sends it to everyone in the room)
export const broadcastMessage = async (
  meetingId: string,
  sensorId: string,
  content: string
): Promise<void> => {
  await db.ref(`data/chats/${meetingId}/${sensorId}/message`).push({
    msg: content,
    receiver: 0,
  });
};

export const getCurrentAssignment = async (
  meetingId: string
): Promise<RoomAssignment> => {
  const roomAssignments = await getValFromDb(
    `config/${meetingId}/current/currentState/plugins/task/breakout rooms/assignment`
  );

  return roomAssignments ?? {};
};

// Given a meeting ID and a room name, returns whether there is a room
// with that name in the current breakout room assignment for that meeting
export const roomWithNameExists = async (
  meetingId: string,
  roomName: string
): Promise<boolean> => {
  const assignment = await getCurrentAssignment(meetingId);
  return Boolean(assignment[roomName]);
};

export const updateAssignmentName = async (
  meetingId: string,
  oldName: string,
  newName: string
): Promise<ErrorOr<void>> => {
  const assignment = await getCurrentAssignment(meetingId);

  const oldRoom = assignment[oldName];
  if (!oldRoom) {
    return err(
      `[${meetingId}]: Trying to rename assignment that doesn't exist ("${oldName}")`
    );
  }

  const newAssignment = Object.fromEntries(
    Object.entries(assignment)
      .filter(([roomName]) => roomName !== oldName)
      .concat([[newName, oldRoom]])
  );

  await setBreakoutRoomAssignment(meetingId, newAssignment);

  return succ(undefined);
};

// Delete message with given index
export const deleteMessage = async (
  meetingId: string,
  messageIndex: number
): Promise<void> => {
  await db
    .ref(
      `/config/${meetingId}/current/currentState/plugins/task/messages/${messageIndex}`
    )
    .remove();
};

export const messageExists = async (
  meetingId: string,
  messageIndex: number
): Promise<boolean> => {
  return (
    await db
      .ref(
        `config/${meetingId}/current/currentState/plugins/task/messages/${messageIndex}`
      )
      .get()
  ).exists();
};

export const scheduleSendMessage = async (
  timestamp: number,
  meetingId: string,
  messageIndex: number
) => {
  const jobId = await createScheduledFirebaseSet(db, timestamp, meetingId,
    `config/${meetingId}/current/currentState/plugins/task/messages/${messageIndex}/send`, true);

  await db.ref(
    `config/${meetingId}/current/currentState/plugins/task/messages/${messageIndex}/sendJobId`
  ).set(jobId);
};

export const cancelScheduledSendMessage = async (
  meetingId: string,
  messageIndex: number
) => {
  const jobIdSnapshot = await db.ref(
    `config/${meetingId}/current/currentState/plugins/task/messages/${messageIndex}/sendJobId`
  ).get();

  if (jobIdSnapshot.exists()) {
    const jobId = jobIdSnapshot.val();
    await cancelScheduledFirebaseSet(db, meetingId, jobId);
    await db.ref(
      `config/${meetingId}/current/currentState/plugins/task/messages/${messageIndex}/sendJobId`
    ).remove();
  }
}
