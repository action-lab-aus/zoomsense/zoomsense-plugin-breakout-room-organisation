import { ZoomSenseCurrentUserInMeeting } from "@zoomsense/zoomsense-firebase";

import { RoomAssignment } from "../../src-shared";

/**
 * Given two arrays, zip them together, with
 * extras wrapping around.
 *
 * e.g.:
 * wrappingZip([1,2],['a','b']) => [[1,'a'],[2,'b']]
 * wrappingZip([1,2],['a','b','c']) => [[1,'a','c'],[2,'b']]
 * wrappingZip([1,2,3],['a','b']) => [[1,3,'a'],[2,'b']]
 *
 * @param {any[]} arrA
 * @param {any[]} arrB
 * @return {any[]}
 */
export const wrappingZip = <T>(arrA: T[], arrB: T[]): T[][] => {
  const numPairs = Math.min(arrA.length, arrB.length);

  const result = new Array(numPairs).fill(null);

  arrA.forEach((el, i) => {
    const index = i % result.length;
    if (!result[index]) {
      result[index] = [];
    }
    result[index].push(el);
  });

  arrB.forEach((el, i) => {
    const index = i % result.length;
    if (!result[index]) {
      result[index] = [];
    }
    result[index].push(el);
  });

  return result;
};

// Given a user and a list of users, return
// whether the user exists in the list
export const inUserList =
  (userList: ZoomSenseCurrentUserInMeeting[]) =>
  (user: ZoomSenseCurrentUserInMeeting): boolean =>
    userList.findIndex((el) => el.username === user.username) >= 0;

// Rotate `arr` `n` times
// rotateArray([1,2,3], 1) => [2,3,1]
// rotateArray([1,2,3], 2) => [3,1,2]
export const rotateArray = <T>(arr: T[], n: number): T[] => {
  const copy = [...arr];
  for (let i = 0; i < n; i++) {
    const newEl = copy.shift();
    if (newEl) copy.push(newEl);
  }
  return copy;
};

// Given an array of values, return an array of pairs
// of every second value.
//
// Examples:
// pairArray([1,2,3,4]) => [[1,2],[3,4]]
// pairArray(['a','b','c','d','e']) => [['a','b'],['c','d'],['e']]
export const pairArray = <T>(arr: T[]): T[][] =>
  arr.reduce((acc, _, index, array) => {
    if (index % 2 === 0) acc.push(array.slice(index, index + 2));
    return acc;
  }, [] as T[][]);

// Given an assignment, and a list of users, return a new assignment
// where the given users are spread out across the rooms in the assignment
export const spreadUsersAcrossRooms = (
  usersRef: ZoomSenseCurrentUserInMeeting[],
  roomsRef: RoomAssignment
): RoomAssignment => {
  // We duplicate the parameters, because we will be modifying them
  // in place and don't want to mutate the caller's variables
  const users = JSON.parse(JSON.stringify(usersRef)) as ZoomSenseCurrentUserInMeeting[];
  const rooms = JSON.parse(JSON.stringify(roomsRef)) as RoomAssignment;

  // If there are no rooms to spread the users across, let's just put
  // them all in a new room
  if (!Object.keys(rooms).length) {
    return { "Room 1": users };
  }

  const roomNames = Object.keys(rooms);
  let roomToAssign = 0;
  while (users.length) {
    const roomName = roomNames[roomToAssign % roomNames.length];

    rooms[roomName].push(users.pop()!);
    roomToAssign += 1;
  }

  return rooms;
};
