import { ZoomSenseCurrentUserInMeeting } from "@zoomsense/zoomsense-firebase";

import { getCurrentAttendeeUserList } from "../getCurrentAttendeeUserList";
import { RoomAssignment, Topology } from "../../src-shared";

// Generate a random breakout room config. Config should have
// a `room_count` property that describes how many rooms there should
// be, or a `participants_per_room` property which creates enough
// rooms to try to have the nominated number of participants in each.
export const generateRandomBreakoutRooms = async (
  meetingId: string,
  topology: Topology
): Promise<RoomAssignment> => {
  const userList = await getCurrentAttendeeUserList(meetingId);

  // Here, we shuffle (randomise) the order of the
  // user list
  for (let i = userList.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    const temp = userList[i];
    userList[i] = userList[j];
    userList[j] = temp;
  }

  if (topology.room_count) {
    return splitIntoRoomCount(userList, topology.room_count);
  } else if (topology.participants_per_room) {
    return splitIntoParticipantCount(userList, topology.participants_per_room);
  }

  // No Room Count or Participants Per Room specified
  return { "Room 1": userList };
};

// Given a list of participants, split the list into rooms, such that each
// room has as close to `participantsPerRoom` participants as possible
const splitIntoParticipantCount = (
  userList: ZoomSenseCurrentUserInMeeting[],
  participantsPerRoom: number
): RoomAssignment => {
  const roomCount = Math.ceil(userList.length / participantsPerRoom);

  return Object.fromEntries(
    userList
      .reduce(
        (acc, el, i) => {
          acc[i % roomCount].push(el);
          return acc;
        },
        new Array(roomCount).fill(0).map(() => [] as ZoomSenseCurrentUserInMeeting[])
      )
      .map((el, ind) => [`Room ${ind + 1}`, el])
  );
};

// Given a list of participants, split the list into
// `roomCount` rooms
const splitIntoRoomCount = (
  userList: ZoomSenseCurrentUserInMeeting[],
  roomCount: number
): RoomAssignment => {
  //
  // We iterate from `room_count` to 1,
  // each time, taking `user_count / i` users
  // and creating a breakout room from them.
  //
  // So, if we were making 3 rooms from 9 users,
  // we would take 9 / 3 = 3, and create a room from
  // those 3 users. Then, we would take 6 / 2 = 3, and
  // create a room from that. then we take 3 / 1.
  //
  const roomCountToOne = new Array(roomCount)
    .fill(0)
    .map((_, i, arr) => arr.length - i);
  const result = roomCountToOne.map((i) =>
    userList.splice(0, Math.ceil(userList.length / i))
  );

  return Object.fromEntries(result.map((el, ind) => [`Room ${ind + 1}`, el]));
};
