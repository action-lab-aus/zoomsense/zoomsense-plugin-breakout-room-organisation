import { ZoomSenseCurrentUserInMeeting } from "@zoomsense/zoomsense-firebase";

import { db } from "../db";
import { getCurrentAttendeeUserList } from "../getCurrentAttendeeUserList";
import { inUserList, spreadUsersAcrossRooms } from "./util";
import { RoomAssignment, Topology } from "../../src-shared";

/**
 * Generate the repeat breakout room assignment. This function generates
 * breakout room assignment that is the same as a previous script section.
 *
 * It filters out participants that are no longer in the zoom call though
 *
 * @param {string} meetingId the meeting to assign users within
 * @param {*} topology topology for the breakout rooms
 */
export const generateRepeatBreakoutRooms = async (
  meetingId: string,
  topology: Topology
): Promise<RoomAssignment> => {
  const script = (
    await db.ref(`config/${meetingId}/schedule/schedule/script`).get()
  ).val();

  if (!script) {
    // TODO: Consider throwing an error rather than returning an empty object
    return {};
  }

  // Find the section index of the section to repeat
  const toRepeat = script.findIndex(
    (section) => section.name === topology.same_as
  );

  if (toRepeat < 0) return {};

  const assignmentHistory = (
    await db.ref(`/data/plugins/task/${meetingId}/breakout rooms`).get()
  ).val();
  if (!assignmentHistory) {
    return {};
  }

  // Get the breakout room assignment for the section to repeat
  const { assignment } =
    Object.values(assignmentHistory)
      .sort((a, b) => b.timestamp - a.timestamp)
      .find((a) => a.section === toRepeat) || {};

  if (!assignment) {
    throw new Error(`Couldn't find section "${toRepeat}" in meeting ${meetingId} assignment history to repeat.`);
  }

  const currentUserList = await getCurrentAttendeeUserList(meetingId);
  // Function for checking if a given user is still in the zoom call
  const isCurrent = inUserList(currentUserList);

  const rooms = Object.fromEntries(
    Object.entries(assignment)
      .map(([roomName, userList]): [string, ZoomSenseCurrentUserInMeeting[]] => [
        roomName,
        userList.filter(isCurrent),
      ])
      .filter(([, userList]) => userList.length)
  );

  // Flat list of all users that we have assigned so far
  const assignedUsers = Object.values(rooms).flat();
  // Of all participants in the room so far, which of them
  // haven't been assigned a room
  const participantsNotAssigned = currentUserList.filter(
    (user) =>
      assignedUsers.findIndex(
        (assignedUser) => assignedUser.username === user.username
      ) === -1
  );

  // Remove users who are no longer in the meeting and remove empty rooms
  return spreadUsersAcrossRooms(participantsNotAssigned, rooms);
};
