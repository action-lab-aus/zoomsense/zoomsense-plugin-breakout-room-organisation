import { db } from "../db";
import {
  pairArray,
  inUserList,
  rotateArray,
  wrappingZip,
  spreadUsersAcrossRooms,
} from "./util";
import { getCurrentAttendeeUserList } from "../getCurrentAttendeeUserList";
import { AssignmentHistoryEntry, RoomAssignment, Topology } from "../../src-shared";

/**
 * Search backwards for the most recent user assignment created by a different topology of the Task plugin.
 * @param {string} meetingId The meeting whose user assignments should be searched.
 * @param {Topology} topology The current Task plugin topology. The result must have be created by a different topology.
 * @return {Promise<AssignmentHistoryEntry | undefined>} The most recent breakout room assignment in the history created
 * by the Task plugin using a different topology to the one provided.
 */
async function getPreviousDifferentTask(
  meetingId: string,
  topology: Topology
): Promise<AssignmentHistoryEntry | undefined> {
  const script = (
    await db.ref(`config/${meetingId}/schedule/schedule/script`).get()
  ).val();
  if (!script) {
    // TODO: Consider throwing an error rather than returning undefined
    return undefined;
  }

  const assignmentHistory = (
    await db.ref(`data/plugins/task/${meetingId}/breakout rooms`).get()
  ).val();
  if (!assignmentHistory) {
    return undefined;
  }

  const lastAssignment = Object.values(assignmentHistory || {})
    .sort((a, b) => b.timestamp - a.timestamp)
    .find((assignment) => {
      const breakoutRoomConfig = script[assignment.section]?.plugins?.find(
        (p) => p.plugin === "Task"
      );
      if (!breakoutRoomConfig) return false;
      return (
        (breakoutRoomConfig.settings as Record<string, { type: string }>)[
          "breakout rooms"
        ]?.type !== topology.type
      );
    });
  console.log("Last assignment: ", JSON.stringify(lastAssignment));

  if (!lastAssignment || !lastAssignment.assignment) {
    throw new Error(
      "Task plugin cannot use pairsRoundRobin type for the first breakout room for the meeting"
    );
  }
  return lastAssignment;
}

/**
 * Generate the breakoutroom assignment for pairs rotation transition.
 * This topology consists of pairing up users, and rotating, so that
 * each user sees a different user. Users shouldn't be with other users
 * that were in their breakout room before the pairs rotation
 *
 * Before Pairs Rotation:
 * Room 1: 'Person A', 'Person B', 'Person C'
 * Room 2: 'Person D', 'Person E', 'Person F'
 *
 * Pairs Rotation 1:
 * Pair 1: 'Person A', 'Person D'
 * Pair 2: 'Person B', 'Person E'
 * Pair 3: 'Person C', 'Person F'
 *
 * Pairs Rotation 2:
 * Pair 1: 'Person C', 'Person D'
 * Pair 2: 'Person A', 'Person E'
 * Pair 3: 'Person B', 'Person F'
 *
 * @param {string} meetingId id of the meeting
 * @param {Topology} topology The topology of this transition step.
 */
export async function generatePairsRoundRobinBreakoutRooms(
  meetingId: string,
  topology: Topology
): Promise<RoomAssignment> {
  const lastAssignment = await getPreviousDifferentTask(meetingId, topology);

  // Short circuit for when we don't have a previous non-transition assignment
  if (!lastAssignment) {
    return {};
  }

  const currentSection = (
    await db.ref(`config/${meetingId}/current/currentSection`).get()
  ).val();
  if (currentSection === null) {
    return {};
  }

  const rotationDistance = currentSection - lastAssignment.section - 1;

  // This is a list of pairs of names
  // Example: [['Room 1', 'Room 2'], ['Room 3', 'Room 4']]
  const namesOfRoomsToBePaired = pairArray(
    Object.keys(lastAssignment.assignment)
  ).filter((el) => el.length === 2);

  const currentUserList = await getCurrentAttendeeUserList(meetingId);
  const isCurrent = inUserList(currentUserList);

  // Get the actual rooms, filter out participants that are no longer
  // in the meeting
  const roomsToBePaired = namesOfRoomsToBePaired.map(
    ([roomNameA, roomNameB]) => {
      return [
        lastAssignment.assignment[roomNameA].filter(isCurrent),
        lastAssignment.assignment[roomNameB].filter(isCurrent),
      ];
    }
  );

  // Rotate the smaller of each of the pairs of rooms
  const roomsToBePairedRotated = roomsToBePaired.map(([roomA, roomB]) => {
    if (roomA.length < roomB.length) {
      return [rotateArray(roomA, rotationDistance), roomB];
    } else {
      return [roomA, rotateArray(roomB, rotationDistance)];
    }
  });

  // Create the rooms by doing a wrapping zip on each of the pairs of rooms
  const rooms = Object.fromEntries(
    roomsToBePairedRotated.flatMap(([roomA, roomB], groupNum) =>
      wrappingZip(roomA, roomB).map((el, i) => [
        // A for the first room, B for the second etc...
        `Pair ${String.fromCharCode(groupNum + 65)}${i + 1}`,
        el,
      ])
    )
  );

  // Flat list of all users that we have assigned so far
  // (the key insight is that this is missing users that
  // have joined since the pairs rotation started)
  const assignedUsers = Object.values(rooms).flat();

  // Of all participants in the room so far, which of them
  // haven't been assigned a room
  const participantsNotAssigned = currentUserList.filter(
    (user) =>
      assignedUsers.findIndex(
        (assignedUser) => assignedUser.username === user.username
      ) === -1
  );

  // Here, we spread the unassigned users across the pairs.
  // This is to handle the case where a user has joined since
  // the rotation started
  return spreadUsersAcrossRooms(
    rotateArray(participantsNotAssigned, rotationDistance),
    rooms
  );
}
