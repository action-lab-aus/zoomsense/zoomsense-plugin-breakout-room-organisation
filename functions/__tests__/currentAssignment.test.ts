import { assert } from "chai";
import * as admin from "firebase-admin";
import { ZoomSenseCurrentUserInMeeting } from "@zoomsense/zoomsense-firebase";

import { getCurrentAssignment } from "../src/db";

const db = admin.database();

describe("getting the current assignment", () => {
  const TEST_ID = "TEST_ID";
  it("should return an empty object", async () => {
    // Since there is no current assignment, we expect this to be a
    // successful empty object
    const currentAssignmentOrErr = await getCurrentAssignment(TEST_ID);

    assert.deepEqual(currentAssignmentOrErr, {});
  });

  it("should return room with one user", async () => {
    const TEST_USER: ZoomSenseCurrentUserInMeeting = {
      username: "TEST",
      userId: "TEST",
      userRole: "Attendee",
    };
    const TEST_ROOM = { "Test Room": [TEST_USER] };

    await db
      .ref(
        `/config/${TEST_ID}/current/currentState/plugins/task/breakout rooms/assignment`
      )
      .set(TEST_ROOM);

    const currentAssignmentOrErr = await getCurrentAssignment(TEST_ID);

    assert.deepEqual(currentAssignmentOrErr, TEST_ROOM);
  });
});
