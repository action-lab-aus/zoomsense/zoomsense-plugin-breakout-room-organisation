import {
  ZoomSenseConfigMeetingScheduleScheduleScript,
  ZoomSenseScheduleAdvanceType
} from "@zoomsense/zoomsense-firebase";
import { assert } from "chai";

import {
  addCurrentUser,
  createTestUsers,
  enablePlugin,
  makeAssignmentChangeSnapshot,
  makeSectionLoadedSnapshot,
  removeCurrentUser,
  setBOConfig,
  setCurrentSection,
  setScript,
  TEST_USERS,
} from "./common";
import { updateAssignmentHistory as rawUpdateAssignmentHistory } from "../src/history";
import { nextSection as rawNextSection } from "../src/nextSection";
import { testRunner } from "./setup";
import { db } from "../src/db";
import { RoomAssignment, Topology } from "../src-shared";

const updateAssignmentHistory = testRunner.wrap(rawUpdateAssignmentHistory);
const nextSection = testRunner.wrap(rawNextSection);

describe("pairs rotation", () => {
  const TEST_ID = "TEST_ID";
  it("should pair up users", async () => {
    const script = [...TEST_SCRIPT];
    script[0] = {
      advance: { type: ZoomSenseScheduleAdvanceType.manual },
      name: "Section 1",
      plugins: [
        {
          plugin: "Task",
          settings: {
            "breakout rooms": {
              type: "random",
              room_count: 2,
            },
          },
        },
      ],
    };
    await setScript(TEST_ID, script);
    // Join 4 users
    await Promise.all(TEST_USERS.slice(0, 4).map((u) => addCurrentUser(TEST_ID, u)));
    await enablePlugin(TEST_ID);

    const randomAssignment = await assignForSectionWithHistoryChange(
      TEST_ID,
      script,
      0
    );
    if (!randomAssignment) assert.fail("Failed to make random assignment");

    const firstRotationAssignment = await assignForSectionWithHistoryChange(
      TEST_ID,
      script,
      1
    );
    if (!firstRotationAssignment) {
      assert.fail("Failed to do first pairs rotation");
    }

    const secondRotationAssignment = await assignForSectionWithHistoryChange(
      TEST_ID,
      script,
      2
    );
    if (!secondRotationAssignment) {
      assert.fail("Failed to do second pairs rotation");
    }

    // 2 Rooms
    assert.equal(Object.keys(firstRotationAssignment).length, 2);
    // The first person from room 1 should be paired
    // with the first person from room 2
    assert.deepEqual(
      firstRotationAssignment["Pair A1"][1],
      randomAssignment["Room 1"][1]
    );
    assert.deepEqual(
      firstRotationAssignment["Pair A1"][2],
      randomAssignment["Room 2"][1]
    );
    // The second person from room 1 should be paired
    // with the second person from room 2
    assert.deepEqual(
      firstRotationAssignment["Pair A2"][1],
      randomAssignment["Room 1"][2]
    );
    assert.deepEqual(
      firstRotationAssignment["Pair A2"][2],
      randomAssignment["Room 2"][2]
    );

    // 2 Rooms
    assert.equal(Object.keys(secondRotationAssignment).length, 2);
    // The first person from room 1 should be paired
    // with the second person from room 2
    assert.deepEqual(
      secondRotationAssignment["Pair A1"][1],
      randomAssignment["Room 1"][1]
    );
    assert.deepEqual(
      secondRotationAssignment["Pair A1"][2],
      randomAssignment["Room 2"][2]
    );
    // The second person from room 1 should be paired
    // with the first person from room 2
    assert.deepEqual(
      secondRotationAssignment["Pair A2"][1],
      randomAssignment["Room 1"][2]
    );
    assert.deepEqual(
      secondRotationAssignment["Pair A2"][2],
      randomAssignment["Room 2"][1]
    );
  });

  it("should work with four rooms", async () => {
    await setScript(TEST_ID, TEST_SCRIPT);
    await Promise.all(createTestUsers(16).map((u) => addCurrentUser(TEST_ID, u)));
    await enablePlugin(TEST_ID);
    const randomAssignment = await assignForSectionWithHistoryChange(
      TEST_ID,
      TEST_SCRIPT,
      0
    );
    if (!randomAssignment) assert.fail("Failed to make random assignment");

    const firstRotationAssignment = await assignForSectionWithHistoryChange(
      TEST_ID,
      TEST_SCRIPT,
      1
    );
    if (!firstRotationAssignment) {
      assert.fail("Failed to do first pairs rotation");
    }

    const secondRotationAssignment = await assignForSectionWithHistoryChange(
      TEST_ID,
      TEST_SCRIPT,
      2
    );
    if (!secondRotationAssignment) {
      assert.fail("Failed to do first pairs rotation");
    }

    assert.equal(Object.keys(randomAssignment).length, 4);
    assert.equal(Object.keys(secondRotationAssignment).length, 8);
    const roomOne = randomAssignment["Room 1"];
    const roomTwo = randomAssignment["Room 2"];
    roomOne.forEach((roomOneUser, index) => {
      if (index === 0) {
        // Ignore the sensor (first user in each room)
        return;
      }
      const roomTwoUser = roomTwo[index];
      assert.deepEqual(
        roomOneUser,
        firstRotationAssignment[`Pair A${index}`][1]
      );
      assert.deepEqual(
        roomTwoUser,
        firstRotationAssignment[`Pair A${index}`][2]
      );
    });
    const roomThree = randomAssignment["Room 3"];
    const roomFour = randomAssignment["Room 4"];
    roomThree.forEach((roomThreeUser, index) => {
      if (index === 0) {
        // Ignore the sensor (first user in each room)
        return;
      }
      const roomFourUser = roomFour[index];
      assert.deepEqual(
        roomThreeUser,
        firstRotationAssignment[`Pair B${index}`][1]
      );
      assert.deepEqual(
        roomFourUser,
        firstRotationAssignment[`Pair B${index}`][2]
      );
    });

    // The first person from room 1 should be paired
    // with the second person from room 2
    assert.deepEqual(
      secondRotationAssignment["Pair A1"][1],
      randomAssignment["Room 1"][1]
    );
    assert.deepEqual(
      secondRotationAssignment["Pair A1"][2],
      randomAssignment["Room 2"][2]
    );
    // The second person from room 1 should be paired
    // with the first person from room 2
    assert.deepEqual(
      secondRotationAssignment["Pair A2"][1],
      randomAssignment["Room 1"][2]
    );
    assert.deepEqual(
      secondRotationAssignment["Pair A2"][2],
      randomAssignment["Room 2"][3]
    );
  });

  it("should handle users leaving", async () => {
    await setScript(TEST_ID, TEST_SCRIPT);

    // Join 8 users
    await Promise.all(createTestUsers(8).map((u) => addCurrentUser(TEST_ID, u)));
    await enablePlugin(TEST_ID);

    const randomAssignment = await assignForSectionWithHistoryChange(
      TEST_ID,
      TEST_SCRIPT,
      0
    );
    if (!randomAssignment) assert.fail("Failed to make random assignment");

    const firstRotationAssignment = await assignForSectionWithHistoryChange(
      TEST_ID,
      TEST_SCRIPT,
      1
    );
    if (!firstRotationAssignment) {
      assert.fail("Failed to do first pairs rotation");
    }

    await removeCurrentUser(TEST_ID, randomAssignment["Room 1"][1]);

    const secondRotationAssignment = await assignForSectionWithHistoryChange(
      TEST_ID,
      TEST_SCRIPT,
      2
    );
    if (!secondRotationAssignment) {
      assert.fail("Failed to do second pairs rotation");
    }

    // 4 Rooms
    assert.equal(Object.keys(firstRotationAssignment).length, 4);
    // The first person from room 1 should be paired
    // with the first person from room 2
    assert.deepEqual(
      firstRotationAssignment["Pair A1"][1],
      randomAssignment["Room 1"][1]
    );
    assert.deepEqual(
      firstRotationAssignment["Pair A1"][2],
      randomAssignment["Room 2"][1]
    );
    // The second person from room 1 should be paired
    // with the second person from room 2
    assert.deepEqual(
      firstRotationAssignment["Pair A2"][1],
      randomAssignment["Room 1"][2]
    );
    assert.deepEqual(
      firstRotationAssignment["Pair A2"][2],
      randomAssignment["Room 2"][2]
    );

    // 4 Rooms
    assert.equal(Object.keys(firstRotationAssignment).length, 4);
    // Three people (plus sensor) in first pairing now
    assert.equal(secondRotationAssignment["Pair A1"].length, 4);
    // The first person from room 2 is now paired with the
    // 4th person from room two and the 3rd person from room
    // 1. This is a "pair" of three since, the first person
    // from room 1 left
    assert.deepEqual(
      secondRotationAssignment["Pair A1"][1],
      randomAssignment["Room 1"][3]
    );
    assert.deepEqual(
      secondRotationAssignment["Pair A1"][2],
      randomAssignment["Room 2"][1]
    );
    assert.deepEqual(
      secondRotationAssignment["Pair A1"][3],
      randomAssignment["Room 2"][4]
    );

    assert.deepEqual(
      secondRotationAssignment["Pair A2"][1],
      randomAssignment["Room 1"][4]
    );
    assert.deepEqual(
      secondRotationAssignment["Pair A2"][2],
      randomAssignment["Room 2"][2]
    );

    assert.deepEqual(
      secondRotationAssignment["Pair A3"][1],
      randomAssignment["Room 1"][2]
    );
    assert.deepEqual(
      secondRotationAssignment["Pair A3"][2],
      randomAssignment["Room 2"][3]
    );
  });
});

// Given a script and a section number, run the assign next section
// for that section number, and write the resulting assignment to
// assignment history
const assignForSectionWithHistoryChange = async (
  meetingId: string,
  script: ZoomSenseConfigMeetingScheduleScheduleScript[],
  section: number
): Promise<RoomAssignment | null> => {
  const settings = script[section].plugins[0].settings as {
    "breakout rooms": Topology;
  };
  const changeParams = {
    params: { meetingId },
  };
  await setBOConfig(meetingId, settings["breakout rooms"]);
  await setCurrentSection(meetingId, section);
  await nextSection(
    makeSectionLoadedSnapshot(meetingId, section),
    changeParams
  );
  const assignmentSnapshot = await db
    .ref(`config/${meetingId}/current/currentState/plugins/task/breakout rooms/assignment`)
    .get();

  if (assignmentSnapshot.exists()) {
    await updateAssignmentHistory(
      makeAssignmentChangeSnapshot(meetingId, assignmentSnapshot.val()),
      changeParams
    );
  }
  return assignmentSnapshot.val();
};

const TEST_SCRIPT: ZoomSenseConfigMeetingScheduleScheduleScript[] = [
  {
    advance: { type: ZoomSenseScheduleAdvanceType.manual },
    name: "Section 1",
    plugins: [
      {
        plugin: "Task",
        settings: {
          "breakout rooms": {
            type: "random",
            participants_per_room: 4,
          },
        },
      },
    ],
  },
  {
    advance: { type: ZoomSenseScheduleAdvanceType.manual },
    name: "Section 2",
    plugins: [
      {
        plugin: "Task",
        settings: {
          "breakout rooms": {
            type: "pairsRoundRobin",
          },
        },
      },
    ],
  },
  {
    advance: { type: ZoomSenseScheduleAdvanceType.manual },
    name: "Section 3",
    plugins: [
      {
        plugin: "Task",
        settings: {
          "breakout rooms": {
            type: "pairsRoundRobin",
          },
        },
      },
    ],
  },
];
