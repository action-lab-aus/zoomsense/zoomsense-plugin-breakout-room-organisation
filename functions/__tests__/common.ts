import {
  ZoomSenseConfigMeetingScheduleScheduleScript,
  ZoomSenseCurrentUserInMeeting
} from "@zoomsense/zoomsense-firebase";
import { makeTypedTestDataSnapshot } from "@zoomsense/zoomsense-firebase-server";

import { RoomAssignment, Topology } from "../src-shared";
import { testRunner } from "./setup";
import { db } from "../src/db";

export const enablePlugin = (meetingId: string): Promise<unknown> =>
  db
    .ref(`config/${meetingId}/current/currentState/plugins/task/enabled`)
    .set(true);

export const setBOConfig = async (
  meetingId: string,
  config: Topology
): Promise<void> => {
  await db
    .ref(
      `config/${meetingId}/current/currentState/plugins/task/breakout rooms`
    )
    .set(config);
};

export const TEST_SENSORS: ZoomSenseCurrentUserInMeeting[] = [
  {
    userId: "",
    username: "ZoomSensor_1",
    userRole: "Sensor"
  },
  {
    userId: "",
    username: "ZoomSensor_2",
    userRole: "Sensor"
  },
];

export const TEST_USERS: ZoomSenseCurrentUserInMeeting[] = [
  {
    userId: "1",
    username: "1",
    userRole: "Attendee",
  },
  {
    userId: "2",
    username: "2",
    userRole: "Attendee",
  },
  {
    userId: "3",
    username: "3",
    userRole: "Attendee",
  },
  {
    userId: "4",
    username: "4",
    userRole: "Attendee",
  },
  {
    userId: "5",
    username: "5",
    userRole: "Attendee",
  },
];

export const createTestUsers = (count: number): ZoomSenseCurrentUserInMeeting[] =>
  new Array(count).fill(0).map((_, i) => ({
    userId: `${i + 1}`,
    username: `${i + 1}`,
    userRole: "Attendee",
  }));

export const addCurrentUser = async (
  meetingId: string,
  user: ZoomSenseCurrentUserInMeeting
): Promise<void> => {
  await db.ref(`data/currentUsers/${meetingId}/ZoomSensor_1/${user.userId}`).set(user);
};

export const removeCurrentUser = async (
  meetingId: string,
  user: ZoomSenseCurrentUserInMeeting
): Promise<void> => {
  await db.ref(`data/currentUsers/${meetingId}/ZoomSensor_1/${user.userId}`).remove();
};

export const setCurrentSection = async (
  meetingId: string,
  section: number
): Promise<void> => {
  await db.ref(`config/${meetingId}/current/currentSection`).set(section);
};

export const makeSectionLoadedSnapshot = (
  meetingId: string,
  section: number
) => {
  return makeTypedTestDataSnapshot(
    testRunner,
    `/config/${meetingId}/current/currentState/sectionLoaded`,
    null,
    section
  );
};

export const makeAssignmentChangeSnapshot = (
  meetingId: string,
  assignment: RoomAssignment
) => {
  return makeTypedTestDataSnapshot(
    testRunner,
    `/config/${meetingId}/current/currentState/plugins/task/breakout rooms/assignment`,
    null,
    assignment
  );
};

export const setScript = async (
  meetingId: string,
  script: ZoomSenseConfigMeetingScheduleScheduleScript[]
): Promise<void> => {
  await db.ref(`config/${meetingId}/schedule/schedule/script`).set(script);
};
