import {
  ZoomSenseCurrentUsersForMeeting,
  ZoomSenseDataLogsBOMetaMeeting,
  ZoomSenseCurrentUserInMeeting
} from "@zoomsense/zoomsense-firebase";
import { RoomAssignment } from "zoomsense-plugin-task-shared";

export interface CurrentBORooms {
  [sensorId: string]: { users: ZoomSenseCurrentUserInMeeting[], isInBO: boolean };
}

/**
 * Augment the usersForMeeting with isInBO from boMeta.
 */
export function getCurrentBORooms(
  usersForMeeting: ZoomSenseCurrentUsersForMeeting,
  boMeta: null | ZoomSenseDataLogsBOMetaMeeting
): CurrentBORooms {
  return Object.fromEntries(
    Object.entries(usersForMeeting)
      .map(([sensorName, users]) => ([
        sensorName,
        {
          users: Object.values(users),
          isInBO: boMeta?.[sensorName]?.current?.isInBO ?? false,
        },
      ]))
  );
}

/**
 * Given the current Breakout Rooms and the current Breakout Room Assignment, return whether the current Breakout
 * Room allocations is correct
 */
export function breakoutsCorrect(
  currentBORooms: CurrentBORooms,
  currentAssignment?: RoomAssignment
): boolean {
  // Transform currentAssignment to a list, so we can search its values
  const assignedRooms = Object.entries(currentAssignment ?? {});
  if (assignedRooms.length === 0) {
    return true;
  }

  // A list of rooms, where a room is just a list of users
  const currentRooms = Object.values(currentBORooms)
    .filter(Boolean)
    .map(({ users }) => users)
    .filter(Boolean);

  // Here we take the `currentRooms` list, and replace each of the
  // users in the inner lists with the names of the rooms they are
  // assigned to
  const correctRooms = currentRooms.map((userList) =>
    userList.map(
      (user) =>
        (assignedRooms.find(
          ([, userList]) =>
            userList.findIndex((other) => other.username === user.username) >= 0
        ) || [])[0]
    )
  );

  // Here we check that for each room, all the users in that
  // room are assigned to the same room
  if (
    !correctRooms.every((rooms) =>
      rooms.every((el, _, arr) => el && el === arr[0])
    )
  )
    return false;

  // Now we check that each of the rooms are assigned to a unique room
  return correctRooms
    .filter((el) => el.length)
    .map((el) => el[0])
    .every((el, i, arr) => el && arr.indexOf(el) === i);
}
