# Zoomsense Plugin - Breakout Room Organisation

- Functions folder contains the firebase functions.

- App folder contains the UI controls (vue-cli-service build --target lib src/library.js) to generate output files into
  the dist dir (notes for CI)

- package.json has metadata on the plugin

# Development

## Functions

In order to expose the plugin functions with the "Task-" prefix (as they are exported in production), change the
`"main"` field in [functions/package.json](functions/package.json) to point to `lib/plugin-index.js` rather
than `lib/index.js` (but don't commit that change.)

Compile the Typescript to Javascript:

`tsc --watch`

The `--watch` flag means the compiler will continue to run and watch for changes to the .ts sources, which will then be
recompiled automatically. Then, in another terminal:

`firebase emulators:start`
or
`npm run emulators`

### Scheduled jobs

The main ZoomSense scheduler won't be running when emulating this plugin locally. To test scheduled jobs, you can run
the firebase shell.

`firebase functions:shell`
or
`npm run shell`

In the shell, you can then type javascript expressions, including manually invoking any function exported from
`index.ts`.

### Automated tests

The firebase mocha tests require the emulators to be running.  The "test" script in `functions/package.json` uses
`firebase emulators:exec` to run them automatically when the test suite is run:

`npm run test`

... but if you want to run tests individually you will need to run the emulator manually:

`firebase emulators:start --only database`

... and if you are already running the emulators, running the "test" script will fail because the ports are already
taken.  Exit your manually-run emulator(s) before running `npm run test` to ensure the full test suite can run without
port conflicts.

## App

`cd ./app/src/components`

Run the [zoomsense-plugin-harness](https://www.npmjs.com/package/@zoomsense/zoomsense-plugin-harness) CLI tool


